---
title: News
author: Jan Tojnar
---
##### 24. 12. 2020
Splits of both stages added to the [results](results.html):

* [Splits of the 1<sup>st</sup> stage](/files/Splits_E1.html)
* [Splits of the 2<sup>nd</sup> stage](/files/Splits_E2.html)

We wish you Merry Christmas and Happy New Year!

##### 15. 12. 2020
Some event photos:

* [1<sup>st</sup> stage](https://runningfox.rajce.idnes.cz/MCR_v_horskem_OB_1_den/)
* [2<sup>nd</sup> stage](https://runningfox.rajce.idnes.cz/MCR_HROB_2den/)
* [Photos by Hrabson](https://photos.app.goo.gl/rGDuuDrrcesToGBA9)

##### 14. 12. 2020
The event is over, we reveal [results](results.html) and maps and routes of both stages:

* [1<sup>st</sup> stage](http://play-map.com/event.php?id=473&lang=english)
* [2<sup>nd</sup> stage](http://play-map.com/event.php?id=474&lang=english)

##### 1. 11. 2020
The COVID situation in the Czech Republic has remained serious and the government has prolonged the state of emergency up to November 20. Therefore another event rescheduling is necessary. New date is December 12-13, a headlamp will be necessary.

##### 14. 10. 2020
The [Bulletin](/en/bulletin_2020.html) of the Czech Mountain Orienteering Champs 2020 has been updated. Besides the new term (November 14-15) we changed starting time of the 1<sup>st</sup> stage.

##### 13. 10. 2020
The Czech government has limited meeting people to 6 persons max. Therefore the event has to be moved to November 14-15. Now I am renegotiating contracts, the updated Bulletin will be issued tomorrow.

##### 10. 10. 2020
We have issued the English [Bulletin](/en/bulletin_2020.html) of the Czech Mountain Orienteering Champs 2020. Due to the Covid situation all sport events in the Czech Republic are stopped for 14 days, including the orienteering in forests. It was promissed the ban may be canceled on October 23, but only in case the COVID numbers are better. Frankly spoken I expect further closures in close future, but preparations are still in progress. We can transform the Champs into unoffitial training, postpone the event or cancel it completely. The final decision will be accepted up to October 26. All the entry payments would be reimbursed.

##### 6. 10. 2020
Mountain Orienteering preparations are in midstream. We are seeking solutions how to organize the event and keep the current COVID restrictions. We are able to start in waves of 50-100 people and maybe the event refreshment will be limited. 

##### 14. 7. 2020
Preparation of the Mountain Orienteering Championships 2020 has been started. The event will be held on October&nbsp;31 and November&nbsp;1&nbsp;2020.<br>
Several check points have been checked last weekend, enjoy [survey photos](survey-summer-2020.html).
