---
title: Results
---

##### Results of the Czech Mountain Orienteering Champs 2020

* [Overall results](/files/vysl_celk.html)
* [Results Stage #1](/files/vysl_e1.html) and [Splits of the 1<sup>st</sup> stage](/files/Splits_E1.html)
* [Results Stage #2](/files/vysl_e2.html) and [Splits of the 2<sup>nd</sup> stage](/files/Splits_E2.html)


##### Maps and routes

* [1<sup>st</sup> stage](http://play-map.com/event.php?id=473&lang=english)
* [2<sup>nd</sup> stage](http://play-map.com/event.php?id=474&lang=english)
