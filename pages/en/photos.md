---
title: Photogallery
---
##### Event photos
* [1<sup>st</sup> stage](https://runningfox.rajce.idnes.cz/MCR_v_horskem_OB_1_den/)
* [2<sup>nf</sup> etapa](https://runningfox.rajce.idnes.cz/MCR_HROB_2den/)
* [Photos by Hrabson](https://photos.app.goo.gl/rGDuuDrrcesToGBA9)

##### Event preparation<br>
* [Check points vetting](pripravy-podzim-2020.html)
* [Terrain Survey 07/2020](survey-summer-2020.html)
