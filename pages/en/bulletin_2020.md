---
title: Bulletin of the Czech Mountain Orienteering Championships 2020 
---
<table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
  <tbody>
    <tr>
      <td style="width:21%;">
          <center><a href="http://www.rogaining.cz/" target="_blank"><img src="/images/car.gif" alt="Logo ČAR"></a></center>
      </td>
      <td colspan="3" style="width:58%;" align="justify">
          <center><h4>Bulletin of the Czech Mountain Orienteering Championships 2020</h4></center>
      </td>
      <td style="width:21%;">
          <center><a href="http://www.caes.cz" target="_blank"><img src="/images/caes-cerna.gif" alt="Logo ČAES"></a></center>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Organizers:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          Krušnohorský rogainingový klub Litvínov
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Date:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>12.–13. 12. 2020</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event site:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>Ústí nad Labem Střekov</strong>, Czech Republic
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event Centre:</strong>
      </td>
      <td colspan="4" style="width:79%;">
       <a href="https://mapy.cz/s/hujozuvafe" target="_blank">Elementary school Karla IV.</a>; GPS: 50°38'54.130"N, 14°3'13.097"E
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event type:</strong>
      </td>
      <td colspan="4" style="width:79%;">               
         <ul>
         <li>Czech Mountain Orienteering Championships 2020</li>
         <li>3<sup>rd</sup> event of the ČAR series 2020</li>
         <li>Public race for newcomers (PZ)</li>
         <li>Public race for skilled O-runners (PO)</li>
         </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event area and terrain:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Event area is bordered by municipalities of Ústí nad Labem Střekov – Velké Březno – Lbín – Sebuzín.<br>
        Western part of Litomeřice highlands, altitude 130–674 m. A densely populated area with a dense network of roads and trails. The landscape is extensively used for agriculture, and there are large fields and pastures.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Map:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Special rogaining map, scale 1: 25 000, e = 10 m, condition 11/2020, printed on waterproof material.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Terms of Participation:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        At the registration desk all competitors have to sign a declaration that they start at their own risk and they are COVID negative and out of any touch with any infected person. For participants under the age of 18, this statement has to be signed by their parent or legal guardian.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
        <strong>Teams and categories:</strong>
    </td>
    <td colspan="4" style="width:79%;">
      A team consists of 2 competitors. All team members must complete the entire race together.<br>
      You can submit your entry for following categories:
        <ul>
        <li>HH20 (CZ Champs men under 20, born after&nbsp;12.&nbsp;12.&nbsp;2000)</li>
        <li>HD20 (CZ Champs mixed under 20, born after&nbsp;12.&nbsp;12.&nbsp;2000)</li>
        <li>DD20 (CZ Champs women under 20, born after&nbsp;12.&nbsp;12.&nbsp;2000)</li>
        <li>HH (CZ Champs men open, no age limit)</li>
        <li>HD (CZ Champs mixed open, no age limit)</li> 
        <li>DD (CZ Champs women open, no age limit)</li>
        <li>HH40 (CZ Champs men above 40, born before&nbsp;12.&nbsp;12.&nbsp;1980)</li>
        <li>HD40 (CZ Champs mixed above 40, born before&nbsp;12.&nbsp;12.&nbsp;1980)</li>
        <li>DD40 (CZ Champs women above 40, born before&nbsp;12.&nbsp;12.&nbsp;1980)</li>
        <li>HH60 (CZ Champs men above 60, born before&nbsp;12.&nbsp;12.&nbsp;1960)</li>
        <li>HD60 (CZ Champs mixed above 60, born before&nbsp;12.&nbsp;12.&nbsp;1960)</li>
        <li>DD60 (CZ Champs women above 60, born before&nbsp;12.&nbsp;12.&nbsp;1960)</li>
        <li>PO (open category for skilled O-runners, no age/gender limits)</li>
        <li>PZ (open category for newcomers, no age/gender limits)</li>
        </ul>
    </td>
      </tr>    
      <tr>
      <td style="width:21%;" valign="top">
          <strong>Entry fee:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
          <tbody>
            <tr>
              <td><strong>Event type</strong></td><td><strong>Basic entry fee per 1 team</strong></td><td><strong>Late entry fee per 1 team</strong></td>
            </tr>
            <tr>
              <td><strong>CZ Champs</strong></td><td>30 €</td><td>30 €</td>
            </tr>
            <tr>
              <td><strong>Open category (PO/PZ) 2 stages</strong></td><td>20 €</td><td>20 €</td>
            </tr>
            <tr>
              <td><strong>Open category (PO/PZ) 1 stage</strong></td><td>10 €</td><td>10 €</td>
            </tr>
          </tbody>
        </table><br>
        Basic entry fee have to be paid up to Sunday 29.&nbsp;11.&nbsp;2020&nbsp;24:00. From Monday November&nbsp;30 we will accept only limited number of&nbsp;entries.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Accommodation:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Limited number of hard floor places at a gym in&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">Hoření street</a>, 5&nbsp;€ per person and night, own sleeping bag.  
      </td>
    </tr>   
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Entries:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Via <a href="https://entries.hrob-2020.rogaining.cz/en/" target="_blank">on-line entry system</a> on&nbsp;the event website.
      </td>
    </tr>
      <td style="width:21%;" valign="top">
          <strong>Payment:</strong>
      </td>
     <td colspan="4" style="width:79%;">
        Entry fee accommodation or SI chip rental pay to the bank account of Krušnohorský rogainingový klub Litvínov.<br>
              Bank address: Fio banka, a.s., V Celnici 1028/10, Praha 1<br> IBAN:<strong>CZ8620100000002800039826</strong><br>
              BIC/SWIFT code: FIOBCZPPXXX
        For payment identification use your team ID.
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Registration:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <strong>Friday 11. 12.</strong> <strong>19:00-22:00</strong> at the&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">gym Hoření street</a><br>
      <strong>Saturday 12. 12.</strong> and <strong>Sunday 13. 12.</strong> <strong>8:00-9:00</strong> at&nbsp;the&nbsp;<a href="https://mapy.cz/s/hujozuvafe" target="_blank">Elementary school Karla IV.</a> Střekov.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Transport and parking:</strong>
      </td>
      <td colspan="4" style="width:79%;">
    Individual car transport or public transport by train to&nbsp;Ústí nad Labem. Due to low parking capacity nearby the event centre please use parking lots specified in the final bulletin.<br>
    For transport from&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">gym Hoření</a> to <a href="https://mapy.cz/s/huhosezeke" target="_blank">event centre</a> (ca&nbsp;6&nbsp;km) a public transport can be used. The connection will be specified in&nbsp;the final bulletin.<br>
    On Saturday arrive in&nbsp;good time (long-distance march to&nbsp;the start of&nbsp;the 1<sup>st</sup> stage).
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Event specification:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Two-stage Mountain Orienteering of couples, time limit of 6&nbsp;hours&nbsp;30&nbsp;minutes (both stages)<br>
        <strong>Saturday 12. 12. 2020:</strong><br>
        Free order with mass start in&nbsp;waves, 1<sup>st</sup> wave starts at&nbsp;9:30.<br>
        <strong>Sunday 13.&nbsp;12.&nbsp;2020:</strong><br>
        Normal orienteering route, teams loosing less than 30 minutes will start in&nbsp;a handicap&nbsp;start beginning at&nbsp;9:30. Other teams in&nbsp;a mass start, will be precised in&nbsp;the final bulletin.
      </td> 
    </tr>   
      <tr>
      <td style="width:21%;" valign="top">
          <strong>Punching system:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      Electronic (SportIdent), each team member must have his/her own SI chip! You can borrow SI chips at the price of 3 € from the organizer, we only accept orders via an electronic registration system. Your chips will wait for you at&nbsp;the registration desk.
      </td>
    </tr>
    <tr>
        <td style="width:21%;" valign="top">
            <strong>Catering:</strong>
        </td>
        <td colspan="4" style="width:79%;">
          Due to the COVID 19 restrictions the refreshment on the route will be limited. We will try our best to provide at least minimal service.
        </td>
        </tr>
     <tr>
        <td style="width:21%;" valign="top">
            <strong>Team evaluation:</strong>
        </td>
        <td colspan="4" style="width:79%;">
        Only teams with all check points punched (both stages) will be classified. A team with shorter time wins (sum of the both stages). 
        </td>
      </tr>
      <tr>
        <td style="width:21%;" valign="top">
            <strong>Event officials:</strong>
        </td>
        <td colspan="4" style="width:79%;">
            Event director: Jan Tojnar<br>
            Course setters: Pavel Kurfürst (Saturday), Jan Tojnar (Sunday)<br>
            Event Referees: Jan Tojnar (Saturday), Pavel Kurfürst (Sunday)
        </td>
      </tr>
      <tr>
        <td style="width:21%;" valign="top">
            <strong>Notice:</strong>
        </td>
        <td colspan="4" style="width:79%;">
            <ul>
            <li>Taking part in the rogaining event is on the own risk of every competitor. All participants have to sign an indemnity form at the registration desk.</li>
            <li>The organizer doesn’t hold responsibility and/or liability for any damage caused by any participant of the race.</li>
            </ul>
        </td>
      </tr>
  </tbody>
</table>