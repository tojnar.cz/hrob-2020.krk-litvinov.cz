---
title: Novinky
author: Jan Tojnar
---
##### 24. 12. 2020
Do [přehledu výsledků](vysledky.html) byly doplněny mezičasy obou etap:

* [Mezičasy 1. etapy](/files/Splits_E1.html)
* [Mezičasy 2. etapy](/files/Splits_E2.html)

Přejeme všem šťastné Vánoce a úspěšný rok 2021!

##### 15. 12. 2020
Ve fotogalerii přibyly fotky od Foxe:

* [1. etapa](https://runningfox.rajce.idnes.cz/MCR_v_horskem_OB_1_den/)
* [2. etapa](https://runningfox.rajce.idnes.cz/MCR_HROB_2den/)

a [fotky od Hrabsona](https://photos.app.goo.gl/rGDuuDrrcesToGBA9)

##### 14. 12. 2020
Do stránky [výsledků](vysledky.html) jsme přidali odkazy pro&nbsp;zákres postupů na&nbsp;Play-Map:

* [Postupy E1](http://play-map.com/event.php?id=473&lang=czech)
* [Postupy E2](http://play-map.com/event.php?id=474&lang=czech)

Prosím všechny přeživší závodníky, by na web nahráli své GPS logy nebo ručně zakreslili postupy v&nbsp;obou etapách, abychom se navzájem poučili, kudy se chodit mělo a kudy nikoli. 

##### 13. 12. 2020
Zveřejnili jsme [celkové výsledky](/files/vysl_celk.html). Prohlédnout si můžete i výsledky [první](/files/vysl_e1.html) a [druhé](/files/vysl_e2.html) etapy.

##### 12. 12. 2020
Na webu [https://liveresultat.orientering.se](https://liveresultat.orientering.se/followfull.php?lang=cz&comp=18613) můžete sledovat výsledky on-line.
A můžete si prohlédnout i [výsledky 1. etapy](/files/vysl_e1.html)

##### 10. 12. 2020
Zveřejňujeme [pokyny](pokyny.html). Ještě dnes pošlu mail s&nbsp;informacemi všem ubytovaným v&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">tělocvičně Hoření</a>. Teď jdu balit. Nashle zítra.

##### 9. 12. 2020
Nejdůležitější změny jsem opravil v&nbsp;[rozpisu](rozpis.html), zveřejnil jsem pár nových [fotek&nbsp;z&nbsp;probíhání&nbsp;kontrol](pripravy-podzim-2020.html), a nyní začínám připravovat pokyny. Prozatím si prohlédněte [délky tratí](/files/Trate_HROB_2020.pdf).

##### 4. 12. 2020
Vše jde po plánu. Teda skoro. Včera jsme si se&nbsp;školníkem potvrdili, že nás nechají prezentovat ve&nbsp;vestibulu školy. Dnes v&nbsp;osm proběhla porada, na&nbsp;které ředitel naší akci zakázal. Operativně jsem aktivoval plán B a C, vyrazil do&nbsp;Ústí, a domluvil [nové shromaždiště](https://mapy.cz/s/homohujavo). Výhodou je, že nás pustí dovnitř do&nbsp;tepla klidně už v&nbsp;sedm, a prý nám natočí i pivo :-). A klidně nás tam nechají i přenocovat. Rozpis časem opravím a v pokynech najdete úplně fšechno.

##### 26. 11. 2020
V neděli má naše vláda rozhodovat, zda přepne na&nbsp;rizikový stav č.&nbsp;3, umožňující uspořádání MČR HROB 2020. Osobně očekávám, že nás ještě nepotěší, bo ministři si stěžují, že jim něco pomalu stoupá.<br>
29. 11. zároveň končí oficiální příjem přihlášek a zajištění noclehu v&nbsp;tělocvičně. Už teď atakujeme limit osob povolených pro&nbsp;vnitřní akce, není prostor pro&nbsp;výjimky. Prosím všechny zájemce o&nbsp;start, aby co nejrychleji zaplatili bezhotovostně, nejpozději v&nbsp;pátek 4.&nbsp;12. všechny přihlášky neplatičů vymažu.<br>
A pak se uvidí. V&nbsp;úterý 8.&nbsp;12. buď nechám vytisknout mapy, nebo začnu postupně vracet vložné. Máme před sebou pár týdnů očekáváni a nadějí, zkrátka advent :-).

##### 14. 11. 2020
Dnes jsme měli startovat první etapu. Stav nouze ale trvá, a vláda ho prý chce prodloužit až do&nbsp;20.&nbsp;prosince. Pokud se dnes zveřejněná tabulka PES nebude měnit podobně chaoticky jako předchozí vládní nařízení, budeme moci HROB uspořádat při dosažení stupně č.&nbsp;3 (sport venku pro&nbsp;50&nbsp;lidí, hospody otevřené do&nbsp;22:00, ubytovací zařízení bez omezení). Nyní je celá republika na&nbsp;70&nbsp;bodech, potřebujeme pokles do&nbsp;pásma 50-60 bodů. Naděje na&nbsp;letošní MČR HROB stále žije, tož modleme se.

---
##### 1. 11. 2020
Tak nám prodloužili nouzový stav. Minimálně do&nbsp;20.&nbsp;listopadu nejspíš nebude možno povečeřet v&nbsp;hospodě, sejít se ve&nbsp;větším počtu nebo uspořádat sportovní akci. Proto jsme se rozhodli HROB opět přesunout. Nový termín 12. až 13.&nbsp;prosince už bude vyžadovat čelovku :-), [rozpis](rozpis.html) už jsem aktualizoval.<br>
Děkuji všem za&nbsp;podporu našich organizačních aktivit, zejména těm, kteří se nově přihlásili. Nic nevzdáváme, zároveň ale každému umožníme odhlášení prostřednictvím&nbsp;mailu: tojnar@gmail.com.

##### 14. 10. 2020
Aktualizoval jsem [rozpis](rozpis.html). Kromě nového termínu (14. až 15. 11.) jsem změnil čas startu první etapy a doplnil informace, jak se na&nbsp;start E1 dostat. Přicházejí první odhlášky, začínáme vracet startovné.<br>
Chci vás všechny ujistit, že pokud se epidemiologická situace nezlepší ani v&nbsp;listopadu, jsme odhodláni přesunout závod třeba na&nbsp;prosinec.

##### 13. 10. 2020
Kvůli zpřísněným vládním opatřením nejsme schopni závod v&nbsp;původním termínu uspořádat. Nepustí nás do&nbsp;tělocvičny ani do&nbsp;školy, v&nbsp;zavřené restauraci se nenavečeříte... Zkusíme akci uspořádat 14.&nbsp;až&nbsp;15.&nbsp;11. Od&nbsp;rána domlouvám přesun s&nbsp;našimi partnery a zítra zveřejním opravený rozpis. Přihlášky zůstávají v&nbsp;platnosti. Prosím týmy, kterým se nový termín nehodí, nebo mají jiný důvod k&nbsp;neúčasti, aby mi napsali na&nbsp;mail: tojnar@gmail.com. Zaplacené startovné jim pošlu zpět na&nbsp;účet.

##### 12. 10. 2020
Včera skončil první termín přihlášek, ještě pár dní ale budeme zapomnětlivcům tolerovat přihlášení za&nbsp;nezvýšené startovné. Vážíme si každého přihlášeného a samozřejmě i všech, kteří nám drží palce, a modlí se za&nbsp;úspěšné uspořádání naší akce. Zázraky se dějí :-).

V&nbsp;nedělní mlze jsme ofáborkovali většinu kontrol, zbytek doufám dorazíme příští víkend. Fotky z&nbsp;fáborkování budu zveřejňovat ve&nbsp;[fotogalerii](pripravy-podzim-2020.html).

##### 10. 10. 2020
Covid stopnul všechny sportovní soutěže na&nbsp;14&nbsp;dní, pořád ale ještě zbývá malá naděje, že se 23. října vše opět uvolní. Čísla nakažených  signalizují spíše další uzávěry, nicméně přípravy stále pokračují. Pokud nevyhlásí zákaz vycházení, můžeme akci převést na&nbsp;neoficiální trénink se startem na&nbsp;krabičku (i ten může mít vítěze). V&nbsp;záloze máme přeložení na&nbsp;listopad (zkrácené tratě nebo start s&nbsp;čelovkou)... Až na&nbsp;posledním místě se krčí úplné zrušení závodu. Finální rozhodnutí zveřejníme do&nbsp;26. října.

##### 7. 10. 2020
Přihlásil se mi Martin Morávek, kterému odpadl kolega na&nbsp;HROB. Rád by si zaběhnul nejraději PZ, PO, max. HH40. Další volný single, Jirka Hájek, preferuje start v kategorii HH/HD40. Zájemci o&nbsp;spárování do&nbsp;týmu (nejen s Martinem a Jirkou), pište na&nbsp;tojnar@gmail.com.

##### 6. 10. 2020
Pokračujeme v přípravách letošního HROBu. Hledáme řešení, jak závod uspořádat za&nbsp;dodržení současně platných omezení, a s&nbsp;napětím očekáváme nařízení budoucí. Klíčový bude limit počtu účastníků. Jsme schopni startovat ve&nbsp;vlnách po&nbsp;100, možná 50 lidech. Místo mačkání v&nbsp;autobuse pro&nbsp;150 osob vás čeká delší pochod na&nbsp;start 1.&nbsp;etapy. Velkou výzvu představuje organizace závodního občerstvení. Pokusíme se zajistit alespoň minimální servis, ale raději nic neslibujeme.

Zrušit naší akci můžeme vždycky, v tom případě bychom zaplacené vklady vrátili.

##### 13. 9. 2020
Nakonec se podařilo zajistit jak pořadatelské ubytování v&nbsp;tělocvičně, ve&nbsp;které se necvičí, tak možnost prezentace a doběhu v&nbsp;areálu ZŠ na&nbsp;Střekově. Můžeme tedy zveřejnit [rozpis](rozpis.html) a spustit [přihláškový systém](https://entries.hrob-2020.rogaining.cz/cs/). A doufat, že se závodu dožijeme.

##### 24. 8. 2020
Těsně před koncem prázdnin už mám obejito téměř vše, fotky některých kontrol i míst, která jsem potkal cestou, najdete ve&nbsp;[fotogalerii](pripravy-leto-2020.html).
Domluvil jsem také místa pro občerstvovací kontroly. Jen vyjednání shromaždiště a noclehu pořád vázne, dneska vás do&nbsp;školy jen tak nepustí.

##### 26. 7. 2020
Víc jak polovinu závodního prostoru a podstatnou část kontrol už mám obhlédnutou. Ve&nbsp;[fotogalerii](pripravy-leto-2020.html) přibylo pár fotek. Vegetace po&nbsp;vydatných deštích bují, ale našel jsem i pár menších [ostrůvků postižených kůrovcem](https://hrob-2020.krk-litvinov.cz/thumbs/hq/files/photos/P1170930.jpg).

##### 14. 7. 2020
Naplno se rozběhly přípravy MČR HROB, který by se měl konat v&nbsp;tradičním dušičkovém termínu 31.&nbsp;10. až 1.&nbsp;11.&nbsp;2020. O&nbsp;kolizi s&nbsp;OB áčkami vím, ale přesun na jiné datum není reálný. Navíc nikdo netuší, jak bude na&nbsp;podzim řádit COVID-19. Nás by nemělo zaskočit ani případné omezení účasti, dokážeme start rozdělit do&nbsp;vln pod&nbsp;100 osob. Jen se trošku obávám, že bude problém se&nbsp;zajištěním shromaždiště a tělocvičny ve&nbsp;škole na&nbsp;Střekově. Pro&nbsp;jistotu sháním tlačenku přes své známé, a záložních plánů mám hned několik :-).<br>
Letos chci cvičně otestovat novinku, vypsání kategorií nad&nbsp;60 let. Není úplně jednoduché vymyslet kratší tratě, které by nebyly úplně stupidní, ale snažil jsem se. Uvidíme, zda budou mít pravdu ti, co mne léta přesvědčovali, že se určitě navýší počet přihlášených veteránů. Tož milí nejzkušenější ze&nbsp;zkušených, záleží jen na&nbsp;vás, zda svou hojnou účastí přimějete budoucí pořadatele k&nbsp;následování mého příkladu.<br>
Po víkendu máme ověřeno prvních 14 kontrol, fotky z&nbsp;průzkumu terénu jsem nahrál do&nbsp;[fotogalerie](pripravy-leto-2020.html).
