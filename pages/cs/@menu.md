[![ČAR](/images/car.gif)](http://www.rogaining.cz/)[![ČAES](/images/caes-cerna.gif)](http://www.caes.cz/)

[![English website](/images/enflag.png)](/en/)

#### Menu

* [Novinky](/cs/)
* [Výsledky](vysledky.html)
* [Fotogalerie](fota.html)
* [Pokyny](pokyny.html)
* [Rozpis](rozpis.html)
* [Přihláškový systém](https://entries.hrob-2020.rogaining.cz/cs/)
* [Informace o počasí](pocasi.html)


#### Další závody ČAR

* [6. Sudetské Tojnárkovo Bloudění (25.-27. 9. 2020)](https://bloudeni.krk-litvinov.cz/2020/cs/)
* [19. Letní Kraktrek (3.-4. 10. 2020)](http://kraktrek.maweb.eu/)
* [Noční můry (16.-17. 10. 2020)](http://mury.play-map.com/)
* 23. Brutus Extreme Orienteering (březen 2021)


#### Partneři závodu

[![Playmap - zákresy postupů](/images/playmap.png)](http://play-map.com/)
