---
title: Rozpis
---
<table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
  <tbody>
    <tr>
      <td style="width:21%;">
          <center><a href="http://www.rogaining.cz/" target="_blank"><img src="/images/car.gif" alt="Logo ČAR"></a></center>
      </td>
      <td colspan="3" style="width:58%;" align="justify">
          <center><h4>Rozpis Mistrovství České republiky v horském orientačním běhu 2020</h4></center>
      </td>
      <td style="width:21%;">
          <center><a href="http://www.caes.cz" target="_blank"><img src="/images/caes-cerna.gif" alt="Logo ČAES"></a></center>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Pořádající orgán:</strong>
      </td>
      <td colspan="4" style="width:79%;">
           <a href="http://www.rogaining.cz/" target="_blank">Česká asociace rogainingu a horského orientačního běhu (ČAR)</a>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Pořádající subjekt:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          Krušnohorský rogainingový klub Litvínov
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Datum:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>12.–13. 12. 2020</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Místo:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>Ústí nad Labem Střekov</strong>, Česká republika
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Centrum závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <a href="https://mapy.cz/s/nepetupume" target="_blank">Klubová restaurace u Karla IV.</a>; GPS: 50°38'53.949"N, 14°3'5.875"E
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Zařazení závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">                
         <ul>
         <li>Mistrovství České republiky v horském orientačním běhu</li>
         <li>3. závod ČAR série 2020</li>
         <li>Veřejný závod pro příchozí</li>
         </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Prostor závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      	Prostor závodu je vymezen obcemi Ústí nad Labem Střekov – Velké Březno – Lbín – Sebuzín.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Terén:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Západní část Litoměřického středohoří, nadmořská výška 130–674 m. Středně hustě osídlená oblast s&nbsp;hustou sítí dopravních komunikací a cest. Krajina je místy zemědělsky využívána, vyskytují se zde rozlehlé pastviny.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Mapa:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Speciálka pro HROB, měřítko 1: 25 000, e = 10 m, stav 12/2020, vodovzdorně upravená, rozměry zveřejníme v&nbsp;pokynech.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Podmínky účasti:</strong>
      </td>
      <td colspan="4" style="width:79%;">
         <ul>
          <li>Všichni závodníci budou na prezentaci podepisovat prohlášení, že startují na&nbsp;vlastní nebezpečí, nejeví známky nakažení COVID 19, ani nepřišli do&nbsp;styku s&nbsp;osobou infikovanou nebo v&nbsp;karanténě. Za účastníky mladší 18 let toto prohlášení podepíše jejich rodič nebo zákonný zástupce. Stáhněte si příslušný <a href="/files/souhlas_18.pdf" target="_blank">formulář</a></li>
          <li>Závodníci mladší 15 let se mohou zúčastnit pouze v&nbsp;doprovodu osoby starší 18 let</li>
          <li>MČR v HROB se smí zúčastnit pouze sportovci registrovaní v&nbsp;ČAR (týká se všech členů týmu). Bezplatná registrace je možná na&nbsp;prezentaci.</li>
          </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Týmy a kategorie:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Tým tvoří 2 osoby. Oba členové týmu musí absolvovat celý závod pohromadě. Při vzdání kteréhokoli člena družstva se musí kompletní tým dostavit do&nbsp;cíle a závod společně ukončit.<br>
        Přihlásit se můžete do následujících kategorií:
        <ul>
        <li>HH20 (MČR dvojic mužů do 20 let, narození po&nbsp;12.&nbsp;12.&nbsp;2000)</li>
        <li>HD20 (MČR smíšených dvojic do 20 let, narození po&nbsp;12.&nbsp;12.&nbsp;2000)</li>
        <li>DD20 (MČR dvojic žen do 20 let, narození po&nbsp;12.&nbsp;12.&nbsp;2000)</li>
        <li>HH (MČR dvojic mužů, bez omezení věku)</li>
        <li>HD (MČR smíšených dvojic, bez&nbsp;omezení věku)</li> 
        <li>DD (MČR dvojic žen, bez omezení věku)</li>
        <li>HH40 (MČR dvojic mužů nad 40 let, narození před&nbsp;12.&nbsp;12.&nbsp;1980)</li>
        <li>HD40 (MČR smíšených dvojic nad 40 let, narození před&nbsp;12.&nbsp;12.&nbsp;1980)</li>
        <li>DD40 (MČR dvojic žen nad 40 let, narození před&nbsp;12.&nbsp;12.&nbsp;1980)</li>
        <li>HH60 (MČR dvojic mužů nad 60 let, narození před&nbsp;12.&nbsp;12.&nbsp;1960)</li>
        <li>HD60 (MČR smíšených dvojic nad 60 let, narození před&nbsp;12.&nbsp;12.&nbsp;1960)</li>
        <li>DD60 (MČR dvojic žen nad 60 let, narození před&nbsp;12.&nbsp;12.&nbsp;1960)</li>
        <li>PO (příchozí orienťáci bez omezení věku a pohlaví, orientačně obtížná trať, nemistrovská kategorie s&nbsp;možností přihlášení pouze na&nbsp;jednu etapu)</li>
        <li>PZ (příchozí začátečníci bez omezení věku a pohlaví, orientačně jednoduchá trať, nemistrovská kategorie s&nbsp;možností přihlášení pouze na&nbsp;jednu etapu)</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Startovné:</strong>
      </td>
      <td colspan="4" style="width:79%;">
  <table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
    <tbody>
      <tr>
        <td><strong>Druh závodu</strong></td><td><strong>Základní startovné za dvojici</strong></td><td><strong>Zvýšené startovné za dvojici</strong></td>
      </tr>
      <tr>
        <td><strong>MČR HROB</strong></td><td>600 Kč</td><td>800 Kč</td>
      </tr>
      <tr>
        <td><strong>Příchozí 2 etapy</strong></td><td>400 Kč</td><td>600 Kč</td>
      </tr>
      <tr>
        <td><strong>Příchozí 1 etapa</strong></td><td>200 Kč</td><td>300 Kč</td>
      </tr>
    </tbody>
  </table><br>
    Základní startovné platí v&nbsp;případě přihlášení a zaplacení do neděle 29.&nbsp;11.&nbsp;2020&nbsp;24:00 hod. Od pondělí 30.&nbsp;11.&nbsp;2020 až do&nbsp;skončení prezentace budeme přihlášky přijímat jen omezeně (dokud nedojdou mapy) a za&nbsp;zvýšené startovné.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Pořadatelské ubytování:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Organizátorům se podařilo zajistit tělocvičnu i se&nbsp;sprchami (vlastní spacák, karimatka) za&nbsp;cenu 100&nbsp;Kč/os. a noc. Ubytování v&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">objektu Hoření</a> je nutno objednat v&nbsp;přihlášce do 29.&nbsp;11.&nbsp;2020.
      </td>
    </tr>   
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Přihlášky:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Výhradně pomocí <a href="https://entries.hrob-2020.rogaining.cz/cs/" target="_blank">on-line přihláškového systému</a> na&nbsp;webu závodu.<br>
        Pro účely vytvoření registru sportovců ČAR bude od závodníků z&nbsp;České republiky systém vyžadovat vyplnění místa trvalého pobytu. Státní orgány budou časem ověřovat soulad registrovaného bydliště s&nbsp;adresou uvedenou v&nbsp;občance.<br>
        Vaše přihláška je zaregistrována v&nbsp;okamžiku, kdy kontaktní osoba (první člen týmu v&nbsp;přihláškovém formuláři) obdrží potvrzení (reply).
      </td>
    </tr>
      <td style="width:21%;" valign="top">
          <strong>Způsob platby:</strong>
      </td>
     <td colspan="4" style="width:79%;">
        Abychom se vyhnuli placení v&nbsp;hotovosti na&nbsp;prezentaci, prosíme o&nbsp;uhrazení vložného (startovné, popř. půjčovné za&nbsp;čipy a objednané nocležné) bankovním převodem na&nbsp;účet Krušnohorského rogainingového klubu Litvínov, vedený u&nbsp;FIO banky pod číslem: <strong>2800039826/2010</strong><br>
        Jako variabilní symbol platby uvádějte ID vašeho týmu, které naleznete v&nbsp;mailu potvrzujícím přijetí vaší přihlášky. Pozor, tento email přijde pouze kontaktní osobě týmu, tj. tomu, kdo je v&nbsp;přihlášce uveden jako první!
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Prezentace:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <strong>Pátek 11. 12.</strong> od <strong>19:00 do 22:00</strong> v&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">areálu Hoření</a><br>
      <strong>Sobota 12. 12.</strong> a <strong>neděle 13. 12.</strong> mezi <strong>8:00 až 9:00</strong> v&nbsp;<a href="https://mapy.cz/s/nepetupume" target="_blank">Klubové restauraci u Karla IV.</a> na&nbsp;Střekově.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Doprava a parkování:</strong>
      </td>
      <td colspan="4" style="width:79%;">
		Vlastními dopravními prostředky nebo hromadnou dopravou. Z&nbsp;Prahy do&nbsp;Ústí vlak jede hodinu a půl, na&nbsp;Střekov se dostanete pěšky nebo MHD. V&nbsp;okolí ZŠ jsou jen omezené parkovací možnosti, využijte parkoviště vyspecifikovaná v&nbsp;pokynech.<br>
    K překonání cca 6&nbsp;km vzdálenosti mezi&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">pořadatelským ubytováním</a> a <a href="https://mapy.cz/s/nepetupume" target="_blank">závodním centrem</a> je možno využít MHD. Vhodné spojení doporučíme v&nbsp;pokynech.<br>
   <!--  K přesunu ze&nbsp;<a href="https://mapy.cz/s/mubutedaso" target="_blank">zastávky Malátova</a>, 600 m pěšky od&nbsp;tělocvičny. -->   
    V sobotu doporučujeme přijet s&nbsp;dostatečnou časovou rezervou, neboť start první etapy leží na&nbsp;kopci 3&nbsp;km od&nbsp;shromaždiště. Alternativně lze využít MHD, bus č.&nbsp;9 z&nbsp;Klíše (odj.&nbsp;8:24 od&nbsp;Hlavního nádraží) do&nbsp;zastávky Nová Ves (příj.&nbsp;8:41). Pořadatel zajistí odvoz svršků, malých baťůžků či tašek do&nbsp;cíle.
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Upřesnění závodu a startů:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Dvouetapový horský orientační běh dvojic s&nbsp;časovým limitem pro&nbsp;obě etapy 6&nbsp;hod.&nbsp;30&nbsp;min.<br>
        <strong>Sobota 12. 12. 2020:</strong><br>
        Free order s hromadným startem ve vlnách, první vlna startuje v&nbsp;9:30.<br>
        <strong>Neděle 13.&nbsp;12.&nbsp;2020:</strong><br>
        Trať s pevným pořadím kontrol. Pozor, změna!!! Nebude hendikepový start, ale opět vlny, první v&nbsp;9:30. Detaily upřesníme v&nbsp;pokynech.
      </td> 
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Systém ražení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Elektronický systém SportIdent. Kontroly včetně cílové budou nastaveny do&nbsp;režimu bezdotykového ražení BEACON, umožňující ražení do&nbsp;vzdálenosti 0.5&nbsp;m.<br>
        Při závodě je možno použít SI čipy řady 5, 6, 8, 9, 10, 11 nebo SIAC.<br>
        <strong>!!! Každý člen týmu musí mít svůj SI čip !!!</strong><br>
        Zapůjčení SI čipu si můžete objednat v přihlášce (půjčovné 60&nbsp;Kč/ks, případě ztráty čipu budeme vybírat 1000&nbsp;Kč).
      </td>
    </tr> 
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Závodní a pozávodní občerstvení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      	Pro každou etapu máme domluveny 2 občerstvovací kontroly, minimálně jednu i pro kratší tratě. Nabídneme pouze nápoje: čaj, rum, bum! :-). V&nbsp;prostoru závodu se nachází několik studánek a pramenů (v&nbsp;mapě vyznačeny modrým kelímkem). Kvůli četným pastvinám a lidským obydlím nedoporučujeme pít vodu z&nbsp;potoků. Většina restaurací v&nbsp;závodním prostoru během víkendu otevírá až odpoledne, večer, nebo vůbec. V&nbsp;cíli dostanete čaj.
      </td>
    </tr>   
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Způsob hodnocení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Aby tým mohl být klasifikován, musí oba jeho členové orazit všechny kontroly obou etap. Pořadí týmů ve výsledkové listině se určuje na&nbsp;základě součtu časů obou etap.<br>
        Vítězové kategorií OPEN obdrží certifikát opravňující k&nbsp;bezplatnému startu na&nbsp;prvním závodě ČAR série 2021, Brutus Extreme Orienteering (tým musí závodit ve&nbsp;stejném složení).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Funkcionáři:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          Ředitel závodu: Jan Tojnar<br>
          Stavitel tratí sobota/neděle: Pavel Kurfürst/Jan Tojnar<br>
          Hlavní rozhodčí sobota/neděle: Jan Tojnar/Pavel Kurfürst
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Informace:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Na webu: <a href="https://hrob-2020.krk-litvinov.cz/cs/" target="_blank">https://hrob-2020.krk-litvinov.cz/cs/</a> nebo na&nbsp;mobilu: <strong>739&nbsp;548&nbsp;142</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Upozornění:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <ul>
        <li>Závodí se podle platných pravidel ČAR. Výjimky z&nbsp;pravidel budou uvedeny na&nbsp;webových stránkách závodu a v&nbsp;pokynech.</li>
        <li>Každý závodník se akce účastní na&nbsp;vlastní nebezpečí a sám zodpovídá za&nbsp;svou bezpečnost, což stvrdí svým podpisem prohlášení na&nbsp;prezentaci. Pořadatel neručí za&nbsp;žádné škody způsobené účastníky akce.</li>
        </ul>
      </td>
      </tr>
      <tr>
      <td style="width:21%;" valign="top">
          <strong>Schvalovací doložka:</strong>
      </td>
      <td colspan="4" style="width:79%;">
       	Rozpis byl schválen Prezidiem ČAR v&nbsp;prosinci 2020.
      </td>
      </tr>
  </tbody>
</table>