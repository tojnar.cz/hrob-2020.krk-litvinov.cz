---
title: Fotogalerie
---
##### Závodní víkend
* [1. etapa - Fox](https://runningfox.rajce.idnes.cz/MCR_v_horskem_OB_1_den/)
* [2. etapa - Fox](https://runningfox.rajce.idnes.cz/MCR_HROB_2den/)
* [Fotky Hrabson](https://photos.app.goo.gl/rGDuuDrrcesToGBA9)

##### Fotky z příprav

* [Obíhání kontrol](pripravy-podzim-2020.html)
* [Průzkum terénu 07/2020](pripravy-leto-2020.html)
