---
title: Fotky z příprav MČR HROB 2020
---

<br>
<br>
<br>
<br>

##### Obhlídky terénu v létě 2020
<div class="figuregroup">
![](../files/photos/P1170989.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170987.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170986.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170985.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170984.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170983.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170982.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170981.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170977.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170976.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170974.jpg){.thumb-sm data-lightbox="o1"}		
![](../files/photos/P1170973.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170972.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170971.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170970.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170969.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170968.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170967.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170966.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170965.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170964.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170961.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170960.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170958.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170956.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170955.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170954.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170953.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170952.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170950.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170949.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170947.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170946.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170945.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170944.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170941.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170930.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170929.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170925.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170919.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170916.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170915.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170904.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170903.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170902.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170900.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170897.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170893.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170892.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170890.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170888.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170887.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170886.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170885.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170884.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170883.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170882.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170881.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170880.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170879.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170878.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170877.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170876.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170875.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170872.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1170869.jpg){.thumb-sm data-lightbox="o1"}
</div>
