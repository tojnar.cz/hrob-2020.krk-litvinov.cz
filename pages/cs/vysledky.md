---
title: Výsledky
---

##### Výsledky MČR HROB 2020

* [Celkové výsledky](/files/vysl_celk.html)
* [Výsledky E1](/files/vysl_e1.html) a [mezičasy E1](/files/Splits_E1.html)
* [Výsledky E2](/files/vysl_e2.html) a [mezičasy E2](/files/Splits_E2.html)


##### Zakreslování postupů na Play-Map:

* [Postupy E1](http://play-map.com/event.php?id=473&lang=czech)
* [Postupy E2](http://play-map.com/event.php?id=474&lang=czech)