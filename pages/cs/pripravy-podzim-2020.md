---
title: Fotky z fáborkování kontrol MČR HROB 2020
---

<br>
<br>
<br>
<br>

##### Fáborkování kontrol na podzim 2020
<div class="figuregroup">
![](../files/photos/P1180074.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180085.jpg){.thumb-sm data-lightbox="o1"}
![Restaurace U Karla IV.](../files/photos/P1180089.jpg){.thumb-sm data-lightbox="o1"}
![Sál shromaždiště](../files/photos/P1180087.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180088.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180092.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180093.jpg){.thumb-sm data-lightbox="o1"}
![Komplexní vybavenost](../files/photos/P1180094.jpg){.thumb-sm data-lightbox="o1"}
![Na kontrole](../files/photos/P1180096.jpg){.thumb-sm data-lightbox="o1"}
![Bacha, medvěd!](../files/photos/P1180098.jpg){.thumb-sm data-lightbox="o1"}
![Těžba dřeva](../files/photos/P1180099.jpg){.thumb-sm data-lightbox="o1"}
![Paseky nedaleko kontroly](../files/photos/P1180100.jpg){.thumb-sm data-lightbox="o1"}
![Dřevo](../files/photos/P1180101.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180102.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180104.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180035.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180034.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180033.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180032.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180030.jpg){.thumb-sm data-lightbox="o1"}
![](../files/photos/P1180029.jpg){.thumb-sm data-lightbox="o1"}
</div>
