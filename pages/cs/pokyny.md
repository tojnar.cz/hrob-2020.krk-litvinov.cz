---
title: Pokyny
---
<table border="1" cellspacing="1" cellpadding="1" style="width:100%;" class="c9" bordercolor="#C0C0C0">
  <tbody>
    <tr>
      <td style="width:21%;">
          <center><a href="http://www.rogaining.cz/" target="_blank"><img src="/images/car.gif" alt="Logo ČAR"></a></center>
      </td>
      <td colspan="3" style="width:58%;" align="justify">
          <center><h4>Pokyny Mistrovství České republiky v horském orientačním běhu 2020</h4></center>
      </td>
      <td style="width:21%;">
          <center><a href="http://www.caes.cz" target="_blank"><img src="/images/caes-cerna.gif" alt="Logo ČAES"></a></center>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Pořádající orgán:</strong>
      </td>
      <td colspan="4" style="width:79%;">
           <a href="http://www.rogaining.cz/" target="_blank">Česká asociace rogainingu a horského orientačního běhu (ČAR)</a>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Pořádající subjekt:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          Krušnohorský rogainingový klub Litvínov
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Datum:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>12.–13. 12. 2020</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Místo:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          <strong>Ústí nad Labem Střekov</strong>, Česká republika
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Centrum závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <a href="https://mapy.cz/s/nepetupume" target="_blank">Klubová restaurace u Karla IV.</a>; GPS: 50°38'53.949"N, 14°3'5.875"E
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Zařazení závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">                
         <ul>
         <li>Mistrovství České republiky v horském orientačním běhu</li>
         <li>3. závod ČAR série 2020</li>
         <li>Veřejný závod pro příchozí</li>
         </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Prostor závodu:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      	Prostor závodu je vymezen obcemi Ústí nad Labem Střekov – Velké Březno – Lbín – Sebuzín.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Terén:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Západní část Litoměřického středohoří, nadmořská výška 130–674 m. Středně hustě osídlená oblast s&nbsp;hustou sítí dopravních komunikací a cest. Krajina je místy zemědělsky využívána, vyskytují se zde rozlehlé pastviny.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Mapa:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Speciálka pro HROB, měřítko 1: 25 000, e = 10 m, stav 12/2020, vodovzdorně upravená, v&nbsp;sobotu rozměry A3, v&nbsp;neděli většina kategorií A4, formát A3 jen pro&nbsp;kategorie HH20, HH, HD, DD a HH40.
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Zvláštní symboly:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <a href="/files/Zvlastni_Symboly.jpg" target="_blank"><img src="/files/Zvlastni_Symboly.jpg" alt="Zvláštní symboly" width="500"></a>
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Popisy:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Popisy kontrol budou vytištěny na závodních mapách.
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Podmínky účasti:</strong>
      </td>
      <td colspan="4" style="width:79%;">
         <ul>
          <li>Všichni závodníci budou na prezentaci podepisovat prohlášení, že startují na&nbsp;vlastní nebezpečí, nejeví známky nakažení COVID 19, ani nepřišli do&nbsp;styku s&nbsp;osobou infikovanou nebo v&nbsp;karanténě.</li>
          <li>Závodníci mladší 15 let se mohou zúčastnit pouze v&nbsp;doprovodu osoby starší 18 let</li>
          <li>MČR v HROB se smí zúčastnit pouze sportovci registrovaní v&nbsp;ČAR (týká se všech členů týmu). Bezplatná registrace je možná na&nbsp;prezentaci.</li>
          </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Týmy a kategorie:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Tým tvoří 2 osoby. Oba členové týmu musí absolvovat celý závod pohromadě. Při vzdání kteréhokoli člena družstva se musí kompletní tým dostavit do&nbsp;cíle a závod společně ukončit.<br>
        <ul>
        <li>HH20 (MČR dvojic mužů do 20 let)</li>
        <li>HD20 (MČR smíšených dvojic do 20 let)</li>
        <li>DD20 (MČR dvojic žen do 20 let)</li>
        <li>HH (MČR dvojic mužů, bez&nbsp;omezení věku)</li>
        <li>HD (MČR smíšených dvojic, bez&nbsp;omezení věku)</li> 
        <li>DD (MČR dvojic žen, bez omezení věku)</li>
        <li>HH40 (MČR dvojic mužů nad 40 let)</li>
        <li>HD40 (MČR smíšených dvojic nad 40 let)</li>
        <li>DD40 (MČR dvojic žen nad 40 let)</li>
        <li>HH60 (MČR dvojic mužů nad 60 let)</li>
        <li>HD60 (MČR smíšených dvojic nad 60 let)</li>
        <li>DD60 (MČR dvojic žen nad 60 let)</li>
        <li>PO (příchozí orienťáci bez omezení věku a pohlaví, orientačně obtížná trať, nemistrovská kategorie s&nbsp;možností přihlášení pouze na&nbsp;1 etapu)</li>
        <li>PZ (příchozí začátečníci bez omezení věku a pohlaví, orientačně jednoduchá trať, nemistrovská kategorie s&nbsp;možností přihlášení pouze na&nbsp;1 etapu)</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Pořadatelské ubytování:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Ubytování v&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">objektu Hoření</a>, pouze pro ty, kteří si je objednali předem.
      </td>
    </tr>      
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Doprava a parkování:</strong>
      </td>
      <td colspan="4" style="width:79%;">
		Vlastními dopravními prostředky nebo hromadnou dopravou. Z&nbsp;Prahy do&nbsp;Ústí vlak jede hodinu a půl, na&nbsp;Střekov se dostanete pěšky nebo MHD. Ulice v&nbsp;okolí shromaždiště poskytují jen omezené parkovací možnosti, nicméně kdo přijede včas, ten uspěje. Větší odstavné plochy najdete v&nbsp;docházkové vzdálenosti, např. u&nbsp;<a href="/files/Parking_Nadrazi.png" target="_blank">nádraží&nbsp;Střekov</a> (0.5 km od centra), popř. u&nbsp;<a href="/files/Parking_Strekov.png" target="_blank">hradu Střekov</a> (1 km od&nbsp;centra, start 2. etapy).<br>
    K překonání cca 6&nbsp;km vzdálenosti mezi&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">pořadatelským ubytováním</a> a <a href="https://mapy.cz/s/nepetupume" target="_blank">závodním centrem</a> je možno využít MHD, do&nbsp;zastávky Šafaříkovo náměstí vás zaveze bus č.&nbsp;9 nebo trolejbusy č.&nbsp;60 a 62.<br>
    V sobotu doporučujeme přijet s&nbsp;dostatečnou časovou rezervou, start první etapy leží na&nbsp;kopci 3&nbsp;km od&nbsp;shromaždiště. Alternativně lze využít MHD, bus č.&nbsp;9 z&nbsp;Klíše (odj.&nbsp;8:24 od&nbsp;Hlavního nádraží) do&nbsp;zastávky Nová Ves (příj.&nbsp;8:41, 500 m na&nbsp;start).
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Prezentace:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <strong>Pátek 11. 12.</strong> od <strong>19:00 do 22:00</strong> v&nbsp;<a href="https://mapy.cz/s/donocurepu" target="_blank">areálu Hoření</a><br>
      <strong>Sobota 12. 12.</strong> a <strong>neděle 13. 12.</strong> mezi <strong>8:00 až 9:00</strong> v&nbsp;<a href="https://mapy.cz/s/nepetupume" target="_blank">Klubové restauraci u Karla IV.</a> na&nbsp;Střekově.
      </td>
    </tr>    
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Šatny a WC:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      V&nbsp;<a href="https://mapy.cz/s/nepetupume" target="_blank">Klubové restauraci u Karla IV.</a> na&nbsp;Střekově si budete moci odložit věci a dojít na&nbsp;toaletu. <strong>Do&nbsp;sálu nevstupujte v&nbsp;závodních botách!</strong>
      <br>
      V&nbsp; místě sobotního startu si můžete ulevit v&nbsp;přilehlém lesíku, v&nbsp;neděli se startuje ve&nbsp;městě, kde je možné max. nenápadné vylulání.
      </td>
    </tr>   
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Upřesnění závodu, startu a cíle:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Dvouetapový horský orientační běh dvojic s&nbsp;časovým limitem pro&nbsp;obě etapy 6&nbsp;hod.&nbsp;30&nbsp;min.<br>
        V sobotu 12.&nbsp;12.&nbsp;2020 <strong>Free order</strong>, vzdálenost na&nbsp;start 3&nbsp;km/300&nbsp;m převýšení po&nbsp;modrých fáborkách. Pořadatel zajistí odvoz svršků, malých baťůžků či tašek do&nbsp;cíle. Volné oděvy napytlujeme.<br>
        V neděli 13.&nbsp;12.&nbsp;2020 <strong>trať s&nbsp;pevným pořadím kontrol</strong>, vzdálenost na&nbsp;start 1&nbsp;km/50&nbsp;m. Startuje se na&nbsp;<a href="/files/Parking_Strekov.png" target="_blank">parkovišti hradu Střekov</a>. Můžete zajet přímo tam, nebo ze&nbsp;shromaždiště dojdete po&nbsp;drátech dolů na&nbsp;konečnou trolejbusu a za&nbsp;kruhovým objezdem se napojíte na&nbsp;červenou značku.<br>
        <strong>Po&nbsp;oba dny hromadný start ve&nbsp;vlnách:</strong>         
        <ul>
        <li>1. vlna (kat. DD20, PO - start 9:30)</li>
        <li>2. vlna (kat. HD40, HH40, HD60 - start 9:40)</li>
        <li>3. vlna (kat. HH20, HD20, PZ, DD, HH - start 9:50)</li>
        <li>4. vlna (kat. HD, DD40, DD60, HH60 - start 10:00)</li>
        </ul>
            Cíl bude pokaždé na shromaždišti.
      </td>
    </tr>  
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Délky tratí:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        <table class="table table-bordered table-sm">
          <thead>
          <tr>
          <th rowspan="2">Kategorie</th>
          <th colspan="2">Sobota</th>
          <th colspan="2">Neděle</th>
          </tr>
          <tr>
          <th>Délka [km]</th>
          <th>Počet kontrol</th>
          <th>Délka [km]</th>
          <th>Počet kontrol</th>
          </tr>
          </thead>
          <tbody>
          <tr>
          <td>HH20</td>
          <td>18.8</td>
          <td>13</td>
          <td>20.79</td>
          <td>13</td>
          </tr>
          <tr>
          <td>HD20</td>
          <td>16.1</td>
          <td>11</td>
          <td>18.97</td>
          <td>10</td>
          </tr>
          <tr>
          <td>DD20</td>
          <td>16.1</td>
          <td>11</td>
          <td>14.48</td>
          <td>12</td>
          </tr>
          <tr>
          <td>HH</td>
          <td>27.7</td>
          <td>18</td>
          <td>25.87</td>
          <td>14</td>
          </tr>
          <tr>
          <td>HD</td>
          <td>23.5</td>
          <td>14</td>
          <td>21.93</td>
          <td>14</td>
          </tr>
          <tr>
          <td>DD</td>
          <td>20.3</td>
          <td>11</td>
          <td>18.21</td>
          <td>11</td>
          </tr>
          <tr>
          <td>HH40</td>
          <td>23.5</td>
          <td>14</td>
          <td>23.43</td>
          <td>17</td>
          </tr>
          <tr>
          <td>HD40</td>
          <td>18.8</td>
          <td>13</td>
          <td>15.84</td>
          <td>8</td>
          </tr>
          <tr>
          <td>DD40</td>
          <td>16.1</td>
          <td>11</td>
          <td>13.27</td>
          <td>8</td>
          </tr>
          <tr>
          <td>HH60</td>
          <td>17.2</td>
          <td>11</td>
          <td>14.07</td>
          <td>8</td>
          </tr>
          <tr>
          <td>HD60</td>
          <td>16.1</td>
          <td>11</td>
          <td>13.97</td>
          <td>9</td>
          </tr>
          <tr>
          <td>DD60</td>
          <td>13.9</td>
          <td>9</td>
          <td>13.34</td>
          <td>7</td>
          </tr>
          <tr>
          <td>PZ</td>
          <td>13.9</td>
          <td>9</td>
          <td>14.21</td>
          <td>5</td>
          </tr>
          <tr>
          <td>PO</td>
          <td>15.9</td>
          <td>9</td>
          <td>13.27</td>
          <td>8</td>
          </tr>
          </tbody>
          </table>
      </td>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Průběh závodu a systém ražení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Elektronický systém SportIdent. Kontroly včetně cílové budou nastaveny do&nbsp;režimu bezdotykového ražení BEACON, umožňující ražení do&nbsp;vzdálenosti 0.5&nbsp;m.<br>
        <strong>!!! Každý člen týmu musí mít svůj SI čip !!!</strong><br>
        Před startem jsou závodníci povinni vynulovat a zkontrolovat si své SI čipy pomocí krabiček Clear a Check. Pořadatelé když tak poradí.
        Průchod kontrolou se označí vložením čipu do razící jednotky SI (musí bliknout a pípnout). Kontrolní stanoviště je vybaveno oranžovobílým lampionem o velikosti 30×30 cm a SI jednotkou zajištěnou ocelovým lankem.<br>
        Ihned po doběhu je každý závodník povinen orazit cílovou SI jednotku a poté si nechat vyčíst čip ve vyčítací jednotce ve stánku prezentace. I v případě, že závod vzdáte, musíte projít cílem a nahlásit se pořadatelům, aby nebylo nutno vyhlašovat celostátní pátrání!
      </td>
    </tr> 
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Závodní a pozávodní občerstvení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      	V každé etapě budou 2 občerstvovací kontroly, minimálně jednu i pro kratší tratě. Nabídneme pouze nápoje (čaj, rum dokud nedojde). Delší kategorie si v&nbsp;sobotu i v&nbsp;neděli mohou na&nbsp;vlastní útraty dopřát pivo ve&nbsp;stánku na&nbsp;sjezdovce u&nbsp;Malečova. Většina restaurací v&nbsp;závodním prostoru ale během víkendu otevírá až odpoledne, večer, nebo vůbec.
        <br>V&nbsp;prostoru závodu se nachází několik studánek a pramenů (v&nbsp;mapě vyznačeny modrým kelímkem). Kvůli četným pastvinám a lidským obydlím nedoporučujeme pít vodu z&nbsp;potoků.
        <br>V&nbsp;cíli dostanete čaj, můžete si koupit i pivo.
      </td>
    </tr>   
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Způsob hodnocení:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Aby tým mohl být klasifikován, musí oba jeho členové orazit všechny kontroly obou etap. Pořadí týmů ve výsledkové listině se určuje na&nbsp;základě součtu časů obou etap.<br>
        Vítězové kategorií OPEN obdrží certifikát opravňující k&nbsp;bezplatnému startu na&nbsp;prvním závodě ČAR série 2021, Brutus Extreme Orienteering (tým musí závodit ve&nbsp;stejném složení).
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Funkcionáři:</strong>
      </td>
      <td colspan="4" style="width:79%;">
          Ředitel závodu: Jan Tojnar<br>
          Stavitel tratí sobota/neděle: Pavel Kurfürst/Jan Tojnar<br>
          Hlavní rozhodčí sobota/neděle: Jan Tojnar/Pavel Kurfürst
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Informace:</strong>
      </td>
      <td colspan="4" style="width:79%;">
        Na webu: <a href="https://hrob-2020.krk-litvinov.cz/cs/" target="_blank">https://hrob-2020.krk-litvinov.cz/cs/</a> nebo na&nbsp;mobilu: <strong>739&nbsp;548&nbsp;142</strong>
      </td>
    </tr>
    <tr>
      <td style="width:21%;" valign="top">
          <strong>Upozornění:</strong>
      </td>
      <td colspan="4" style="width:79%;">
      <ul>
        <li>Závodí se podle platných pravidel ČAR.</li>
        <li>Každý závodník se akce účastní na&nbsp;vlastní nebezpečí a sám zodpovídá za&nbsp;svou bezpečnost, což stvrdí svým podpisem prohlášení na&nbsp;prezentaci.</li>
        <li>Pořadatel neručí za&nbsp;žádné škody způsobené účastníky akce.</li>
        <li>Mějte na paměti, v&nbsp;jaké době akci pořádáme a chovejte se tak, abyste nedostali do&nbsp;problémů sebe ani nikoho jiného.</li>
        </ul>
      </td>
      </tr>
  </tbody>
</table>